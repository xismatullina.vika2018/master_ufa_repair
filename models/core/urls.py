from django.contrib import admin
from django.urls import path, include

from models.core.views import IndexViews

urlpatterns = [
    path('', IndexViews.as_view(), name='index'),

]
