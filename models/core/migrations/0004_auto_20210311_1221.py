# Generated by Django 3.1.6 on 2021-03-11 07:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_photo_is_slider'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='thing_color',
        ),
        migrations.AddField(
            model_name='photo',
            name='thing_color',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='core.color', verbose_name='цвет'),
            preserve_default=False,
        ),
    ]
