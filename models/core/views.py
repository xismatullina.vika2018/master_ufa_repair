from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse


class IndexViews(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):


        return {}



